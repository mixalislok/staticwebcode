package com.amdocs.config;
 
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
//import static java.lang.System.*;

@EnableWebMvc
@Configuration
@ComponentScan({ "com.amdocs.web" })
public class SpringWebConfig extends WebMvcConfigurerAdapter {
	
	public SpringWebConfig(){
		super();
	}
	

	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		//out.println("Hello, world");
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}
	
	@Bean
	public InternalResourceViewResolver viewResolver() {
		//out.println("Hello, world");
		final InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/jsp/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

 
}
